package pages;

import helpers.TestHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * Page Object
 * <p/>
 * Created by tvr on 14.07.15.
 */
public class CreateAccountPage {

    public static final String URL = "https://accounts.google.com/SignUp";

    public static final String CREATE_ACCOUNT_FORM_XPATH = "//form[@id='createaccount']";
    public static final String LANG_SELECT_XPATH = "//select[@id='lang-chooser']";
    public static final String FIRST_NAME_FIELD_XPATH = "//input[@id='FirstName']";
    public static final String LAST_NAME_FIELD_XPATH = "//input[@id='LastName']";
    public static final String EMAIL_FIELD_XPATH = "//input[@id='GmailAddress']";
    public static final String PASSWORD_FIELD_XPATH = "//input[@id='Passwd']";
    public static final String PASSWORD_AGAIN_FIELD_XPATH = "//input[@id='PasswdAgain']";
    public static final String BIRTH_DAY_FIELD_XPATH = "//input[@id='BirthDay']";
    public static final String BIRTH_YEAR_FIELD_XPATH = "//input[@id='BirthYear']";
    public static final String PASSWORD_LINK_XPATH = "//a[@id='PasswdLink']";
    public static final String MONTH_LIST_XPATH = "//div[@id=':0']";
    public static final String GENDER_LIST_XPATH = "//div[@id=':d']";
    public static final String PHONE_FIELD_XPATH = "//input[@id='RecoveryPhoneNumber']";
    public static final String SUBMIT_BUTTON_XPATH = "//input[@id='submitbutton']";

    public static final String EMPTY_FIELD_ERROR_MESSAGE = "You can't leave this empty.";
    public static final String USER_NAME_FIELD_HINT = "You can use letters, numbers and full stops.";
    public static final String PASSWORD_FIELD_HINT = "Use at least 8 characters. Don’t use a password from another site or something too obvious like your pet’s name. Why?";
    public static final String USER_NAME_LENGTH_ERROR_MESSAGE = "Please use between 6 and 30 characters";
    public static final String USER_NAME_SPECIAL_CHARS_ERROR_MESSAGE = "Please use only letters (a-z), numbers and full stops.";
    public static final String USER_NAME_TAKEN_ERROR_MESSAGE = "Someone already has that username. Try another?";
    public static final String STRONG_PASSWORD_MESSAGE = "Creating a strong password";
    public static final String SHORT_PASSWORD_MESSAGE = "Short passwords are easy to guess. Try one with at least 8 characters.";
    public static final String PASSWORD_DID_NOT_MATCH_MESSAGE = "These passwords don't match. Try again?";
    public static final String BIRTH_DAY_FIELD_HINT = "People that you connect with on Google may see the month and day of your birthday. Learn more";
    public static final String INVALID_BIRTH_DAY_ERROR_MESSAGE = "Hmm, the day doesn't look right. Make sure that you use a 2-digit number that is a day of the month.";
    public static final String INVALID_BIRTH_YEAR_FORMAT_ERROR_MESSAGE = "Hmm, the year doesn't look right. Make sure that you use four digits.";
    public static final String INVALID_BIRTH_YEAR_ERROR_MESSAGE = "Hmm, the date doesn't look right. Make sure that you use your actual date of birth.";
    public static final String INVALID_PHONE_FORMAT_ERROR_MESSAGE = "This phone number format is not recognised. Please check the country and number.";

    public static void setLocaleToEnglish(WebDriver driver) {
        Select langSelect = new Select(driver.findElement(By.xpath(LANG_SELECT_XPATH)));
        langSelect.selectByValue("en-GB");
    }

    public static void selectMonthByNumber(TestHelper driver, int mounthNumber) {
        driver.getElementByXpath(MONTH_LIST_XPATH).click();
        driver.getElementByXpath("//div[@id=':" + mounthNumber + "']").click();
    }

    public static void selectFemaleGender(TestHelper driver) {
        driver.getElementByXpath(GENDER_LIST_XPATH).click();
        driver.getElementByXpath("//div[@id=':e']").click();
    }
}
