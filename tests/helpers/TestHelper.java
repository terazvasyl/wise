package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

/**
 * Created by tvr on 14.07.15.
 */
public class TestHelper {

    private WebDriver driver;
    final String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    Random rnd = new Random();

    public WebDriver getDriver() {
        return driver;
    }

    /**
     * @param driver WebDriver instance
     */
    public TestHelper(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getElementByXpath(String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public void waitForPageToLoad(int time) {
        try {
            synchronized (driver) {
                driver.wait(time * 1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void waitForPageToLoad() {
        this.waitForPageToLoad(60);
    }

    public void assertTextPresent(String text) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//body"), text));
    }

    public void assertTextNotPresent(String text) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//body"), text)));
    }

    public String getRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }
}
