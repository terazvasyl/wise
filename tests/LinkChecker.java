import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertTrue;

/**
 * Created by tvr on 04.08.15.
 */
public class LinkChecker {

    private WebDriver driver;
    private static String SERVER_URL = "http://wp.amazon/";
    private static String STAGING_SERVER_URL = "http://app.staging.yaware.com/";
    private SoftAssert softAssert = new SoftAssert();
    private List<String> visited = new ArrayList<String>();

    @BeforeClass
    public void setUpBeforeClass() {
        driver = new FirefoxDriver();
        driver.get(SERVER_URL);
        driver.findElement(By.xpath("//input[@id='email']")).sendKeys("v.teraz+auto@yaware.com");
        driver.findElement(By.xpath("//input[@id='password']")).sendKeys("password");
        driver.findElement(By.xpath("//a[@id='login-submit']")).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() throws MalformedURLException {

    }

    public void getAllLinks(String url) throws MalformedURLException {
        if ((new URL(url).getProtocol().equals("http"))) {
            String e = null;
            driver.navigate().to(url);
            System.out.print("Validating " + url + "...\n");
            List<WebElement> links = driver.findElements(By.tagName("a"));

            for (WebElement element : links) {
                if (element != null && !visited.contains(element.getAttribute("href")) && !element.getAttribute("href").contains("#")
                        && !element.getAttribute("href").contains("http://yaware.com")) {
                    visited.add(element.getAttribute("href"));
                    getAllLinks(element.getAttribute("href"));
                }
            }

        }

    }

    private void isLinkValid(HttpsURLConnection connection) {
        try {
            connection.connect();
            final int responseCode = connection.getResponseCode();
            assertTrue(responseCode < 400, "Link + " + connection.getURL().toString() + " returns code " + responseCode);
            System.out.println(responseCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void isLinkValid(HttpURLConnection connection) {
        try {
            connection.connect();
            final int responseCode = connection.getResponseCode();
            assertTrue(responseCode < 400, "Link + " + connection.getURL().toString() + " returns code " + responseCode);
            System.out.println(responseCode);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void assertLinkValid(URL link) {
        System.out.print("Link " + link.toString() + " validating... ");
        try {
            if (link.getProtocol().equals("http")) {
                HttpURLConnection connection = (HttpURLConnection) link.openConnection();
                isLinkValid(connection);
            } else if (link.getProtocol().equals("https")) {
                HttpsURLConnection connection = (HttpsURLConnection) link.openConnection();
                isLinkValid(connection);
            }

        } catch (IOException e) {
        }
    }


    @AfterClass
    public void tearDownAfterClass() {
        driver.quit();
    }
}
