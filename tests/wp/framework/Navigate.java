package wp.framework;

/**
 * Created by tvr on 22.09.15.
 */
public interface Navigate {
    void go(String URL);
}
