package wp.framework;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import wp.framework.pages.GlobalPage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by tvr on 21.09.15.
 */
public class Driver {
    public static final int TIMEOUT = 15;
    private static WebDriver instance = null;

    public static WebDriver getInstance() {
        if (instance == null) {
            try {
                DesiredCapabilities capability = DesiredCapabilities.firefox();
                instance = new RemoteWebDriver(new URL("http://wp.amazon:4444/wd/hub"), capability);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            instance.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            instance.get(GlobalPage.BASE_URL);
        }
        return instance;
    }

    public static void takeScreenShot() {
        File scrFile = ((TakesScreenshot) instance).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("/tmp/screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static WebDriverWait getWaitObject() {
        return new WebDriverWait(Driver.getInstance(), Driver.TIMEOUT);
    }

    public static void closeBrowser() {
        getInstance().manage().deleteAllCookies();
        instance.navigate().refresh();
    }
}
