package wp.framework.actions;

import wp.framework.helpers.WebHelper;
import wp.framework.pages.LoginPage;

/**
 * Created by tvr on 21.09.15.
 */
public class LoginAction {
    private final String userName;
    private String password;

    public LoginAction(String userName) {
        this.userName = userName;
    }

    public LoginAction withPassword(String password) {
        this.password = password;
        return this;
    }

    public void login() {
        WebHelper.getElementByXPath(LoginPage.LOGIN_INPUT_XPATH).sendKeys(this.userName);
        WebHelper.getElementByXPath(LoginPage.PASSWORD_INPUT_XPATH).sendKeys(this.password);
        WebHelper.getElementByXPath(LoginPage.LOGIN_SUBMIT_INPUT_XPATH).click();
    }
}
