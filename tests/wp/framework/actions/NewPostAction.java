package wp.framework.actions;

import org.openqa.selenium.WebElement;
import wp.framework.Driver;
import wp.framework.helpers.WebHelper;
import wp.framework.pages.NewPostPage;

/**
 * Created by tvr on 22.09.15.
 */
public class NewPostAction {
    private String postTitle;
    private String postBody;

    public NewPostAction(String postTitle) {
        this.postTitle = postTitle;
    }

    public NewPostAction withBody(String postBody) {
        this.postBody = postBody;
        return this;
    }

    public void publish() {
        WebHelper.getElementByXPath(NewPostPage.POST_TITLE_FIELD_XPATH)
                .sendKeys(this.postTitle);
        WebElement bodyFrame = WebHelper.getElementByXPath(NewPostPage.POST_BODY_FRAME_XPATH);
        Driver.getInstance().switchTo().frame(bodyFrame);
        WebHelper.getElementByXPath(NewPostPage.POST_BODY_FIELD_XPATH)
                .sendKeys(this.postBody);
        Driver.getInstance().switchTo().defaultContent();
        WebHelper.getElementByXPath(NewPostPage.POST_PUBLISH_BUTTON_XPATH).click();
    }
}
