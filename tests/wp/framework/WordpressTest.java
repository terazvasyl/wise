package wp.framework;

import org.testng.annotations.BeforeTest;
import wp.framework.pages.LoginPage;

/**
 * Created by tvr on 27.09.15.
 */
public class WordpressTest {

    @BeforeTest
    public void setUp() {
        LoginPage.goTo();
        LoginPage.loginAsDefaultAdmin();
    }
}
