package wp.framework.navigation;

import wp.framework.helpers.WebHelper;
import wp.framework.pages.DashboardPage;

/**
 * Created by tvr on 24.09.15.
 */
public class LeftMenu {

    public enum Posts {
        ;

        public static class AddNew {
            public static void select() {
                WebHelper.getElementByXPath(DashboardPage.POSTS_BUTTON_XPATH).click();
                WebHelper.getElementByXPath(DashboardPage.ADD_NEW_POST_BUTTON_XPATH).click();
            }
        }
    }

    public enum Pages {
        ;

        public static class AllPages {
            public static void select() {
                MenuSelector.select(null);
            }
        }
    }
}
