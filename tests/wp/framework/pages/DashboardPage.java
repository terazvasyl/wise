package wp.framework.pages;

import wp.framework.helpers.WebHelper;

/**
 * Created by tvr on 21.09.15.
 */
public class DashboardPage {

    public static final String DASHBOARD_WELCOME_MESSAGE = "Welcome to WordPress!";
    public static final String DASHBOARD_URL = "wp-admin";

    public static final String POSTS_BUTTON_XPATH = "//a[@href='edit.php']";
    public static final String ADD_NEW_POST_BUTTON_XPATH = "//a[@href='http://wp.amazon/wp-admin/post-new.php']";

    public static boolean isAt() {
        return WebHelper.isTestPresent(DASHBOARD_WELCOME_MESSAGE)
                && WebHelper.isURLContain(DASHBOARD_URL);
    }
}
