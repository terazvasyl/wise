package wp.framework.pages;

import wp.framework.actions.NewPostAction;
import wp.framework.helpers.WebHelper;
import wp.framework.navigation.LeftMenu;

/**
 * Created by tvr on 22.09.15.
 */
public class NewPostPage {

    public static final String URL = "wp-admin/post-new.php";

    public static final String POST_TITLE_FIELD_XPATH = "//input[@id='title']";
    public static final String POST_BODY_FIELD_XPATH = "//body[@id='tinymce']";
    public static final String POST_BODY_FRAME_XPATH = "//iframe[@id='content_ifr']";
    public static final String POST_PUBLISH_BUTTON_XPATH = "//input[@id='publish']";

    public static void goTo() {
        LeftMenu.Posts.AddNew.select();
    }


    public static void goToUsingMenu() {
        WebHelper.getElementByXPath(DashboardPage.POSTS_BUTTON_XPATH).click();
        WebHelper.getElementByXPath(DashboardPage.ADD_NEW_POST_BUTTON_XPATH).click();
    }


    public static NewPostAction createNewPost(String postTitle) {
        return new NewPostAction(postTitle);
    }
}
