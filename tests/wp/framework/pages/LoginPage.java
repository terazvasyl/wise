package wp.framework.pages;

import wp.framework.actions.LoginAction;

/**
 * Created by tvr on 21.09.15.
 */
public class LoginPage extends GlobalPage {

    public static final String URL = "wp-login.php";

    public static final String DEFAULT_LOGIN = "teraz";
    public static final String DEFAULT_PASSWORD = "123yaware";

    public static final String LOGIN_INPUT_XPATH = ".//*[@id='user_login']";
    public static final String PASSWORD_INPUT_XPATH = ".//*[@id='user_pass']";
    public static final String LOGIN_SUBMIT_INPUT_XPATH = ".//*[@id='wp-submit']";

    public static void goTo() {
        GlobalPage.goTo(URL);
    }

    public static LoginAction loginAs(String login) {
        return new LoginAction(login);
    }

    public static void loginAsDefaultAdmin() {
        loginAs(DEFAULT_LOGIN).withPassword(DEFAULT_PASSWORD).login();
    }

}


