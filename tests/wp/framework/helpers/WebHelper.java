package wp.framework.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import wp.framework.Driver;

/**
 * Created by tvr on 21.09.15.
 */
public class WebHelper {

    public static WebElement getElementByXPath(String xpath) {
        return Driver.getInstance().findElement(By.xpath(xpath));
    }

    public void assertTextPresent(String text) {
        Driver.getWaitObject().until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//body"), text));
    }

    public static Boolean isTestPresent(String text) {
        return isTestPresent("//body", text);
    }

    public static Boolean isTestPresent(String elementXpath, String text) {
        return Driver.getWaitObject().until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(elementXpath), text));
    }

    public static Boolean isURLContain(String URL) {
        return Driver.getWaitObject().until(ExpectedConditions.urlContains(URL));
    }

}
