package wp;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import wp.framework.Driver;
import wp.framework.WordpressTest;
import wp.framework.pages.NewPostPage;

import static wp.framework.Driver.closeBrowser;

/**
 * Created by tvr on 22.09.15.
 */
public class NewPostTest extends WordpressTest {

    @Test
    public void testCreateNewPost() {
        NewPostPage.goTo();
        Driver.takeScreenShot();
        NewPostPage.createNewPost("This-is-test-title").
                withBody("this-is-test-body").
                publish();
    }

    @AfterClass
    public void tearDownAfterClass() {
        closeBrowser();
    }


}
