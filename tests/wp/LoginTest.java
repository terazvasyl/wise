package wp;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import wp.framework.WordpressTest;
import wp.framework.pages.DashboardPage;

import static org.testng.Assert.assertTrue;
import static wp.framework.Driver.closeBrowser;

/**
 * Created by tvr on 21.09.15.
 */
public class LoginTest extends WordpressTest {

    @Test
    public void testAdminLoginSuccessed() {
        assertTrue(DashboardPage.isAt(), "Failed to login");
    }

    @AfterClass
    public void tearDownAfterClass() {
        closeBrowser();
    }
}
